using UnityEngine;
using System.Collections;

public class SlidingMenu : MonoBehaviour{
	//public Texture2D [] textures;
	public Texture2D prevButton;
	public Texture2D nextButton;
	
	private GUIStyle blankStyle = new GUIStyle(); //an "empty" style to avoid any of Unity's default padding, margin and background defaults
	//private Rect container = new Rect(0,0,250,211);
	//private Rect container = new Rect(0,0,Screen.width,Screen.height);
	private Rect content = new Rect(0,0,Screen.width, Screen.height);
	private float currentSelection;
	
	private int imageIndex = 0;
	private int maxIndex = 3;
    private Object[] textures;
	void Start()
	{
		textures = Resources.LoadAll("HaCuong", typeof(Texture2D));
	}
	void OnGUI () {		
		if (GUI.Button(new Rect (20,20,80,20), "Main Menu")) {
			Application.LoadLevel ("Menu");
		}		
		Texture2D texture = (Texture2D)textures[imageIndex];
		//Calculate scale
		float tRatio = texture.width / texture.height;
		float sRatio = Screen.width / Screen.height;
		if (tRatio > sRatio) { //scale to image width }
			content.y = (Screen.height - ((float)Screen.width/(float)texture.width) * texture.height) /2;
			content.x = 0;
		} else { //scale to image height 
			content.x = (Screen.width - ((float)Screen.height/(float)texture.height) * texture.width) /2;	
			content.y = 0;
		}
		
		GUI.Label(content,(Texture2D)textures[imageIndex], blankStyle);
		//next button:
		//if(GUI.Button(new Rect(180,140,70,71),nextButton,blankStyle) && target > -content.width+container.width){
		if(GUI.Button(new Rect(Screen.width - 64,Screen.height - 64, Screen.width, Screen.height),nextButton,blankStyle) && imageIndex <maxIndex){
			imageIndex ++;
			//EstablishSlide();
		}
		
		//prev button:
		//if(GUI.Button(new Rect(0,140,70,71),prevButton,blankStyle) && target < 0){
		if(GUI.Button(new Rect(0, Screen.height - 64, 64, Screen.height),prevButton,blankStyle) && imageIndex > 0){
			imageIndex --;
			//EstablishSlide();
		}
		
		//select button:
		//if(GUI.Button(new Rect(0,0,250,211),"",blankStyle)){
		//	Selected();
		//}
	}
		
	void EstablishSlide(){		
		//scroll panel:
		
		//GUI.BeginGroup(container);
		Texture2D texture = (Texture2D)textures[imageIndex];
		//GUI.DrawTexture(content, texture);
		GUI.Label(content,texture, blankStyle);
		//GUI.EndGroup();
		
		
	}

}